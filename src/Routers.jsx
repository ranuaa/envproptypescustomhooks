import React from "react";
import { Route, Switch } from "react-router-dom";
import Homepage from "./Pages/Homepage";

const Routers = () => {
  return (
    <>
    <Switch>
        <Route exact path='/' >
            <Homepage/>
        </Route>
    </Switch>
    </>
  );
};

export default Routers;
