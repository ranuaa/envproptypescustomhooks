import { createTheme, ThemeProvider } from '@mui/material';
import Routers from './Routers';

const theme = createTheme({
  palette:{
    primary: {
      main: process.env.REACT_APP_COLOR_PRIMARY
    }
  }
})

function App() {
  return (
    <ThemeProvider theme={theme}>
    <div className="App">
      <Routers/>
    </div>
    </ThemeProvider>
  );
}

export default App;
