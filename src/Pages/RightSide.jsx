import * as React from 'react';
import Cards from '../Components/Cards';

export default function RightSide({post}) {

  return (
    <div>
      <Cards title={post.title} datePost={post.datePost} description={post.description} img={post.img}  />
    </div>
  );
}