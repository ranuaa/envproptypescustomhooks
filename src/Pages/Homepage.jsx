import { Box } from "@mui/system";
import axios from "axios";
import React, { useEffect, useState } from "react";
import Appbar from "../Components/Appbar";
import LeftList from "./LeftList";
import RightSide from "./RightSide";
import {useAsync} from 'react-use'



const Homepage = () => {
    const [posts, setPosts] = useState()
    
    const handleFetch = useAsync(async () => {
        const data = await axios.get(process.env.REACT_APP_API);
        const waitFor = delay => new Promise(resolve => setTimeout(resolve, delay));
        await waitFor(5000);
        setPosts(data.data)
   }, []) 

 return (
  <div>
   <Appbar />
   <Box
    sx={{
     width: "80%",
     margin: "0 auto",
     display: "flex",
     maxWidth: '1200px',
     height: '100%'
    }}>
    {handleFetch.loading ?  <Box
    className='right'
     style={{
      width: "75%",
      display: 'flex',
      flexDirection: "column",
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center'
     }}>
        <h1 style={{marginTop: "15%"}}>Loading</h1>
        <lottie-player src={"https://assets2.lottiefiles.com/packages/lf20_poqmycwy.json"}  background="transparent"  speed="1"  style={{width: "300px", height: "300px"}}  loop  autoplay></lottie-player>
        </Box>
    : handleFetch.error ?  <Box
    className='right'
     style={{
      width: "75%",
      display: 'flex',
      flexWrap: "wrap",
      gap: '10px',
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center'
     }}>
        <h1 style={{marginTop: "15%"}}>{handleFetch.error.message}</h1>
        <lottie-player src={"https://assets9.lottiefiles.com/packages/lf20_pNx6yH.json"}  background="transparent"  speed="1"  style={{width: "300px", height: "300px"}}  loop  autoplay></lottie-player>
        </Box>
    :    <Box
    className='right'
     style={{
      width: "75%",
      display: 'flex',
      flexWrap: "wrap",
      gap: '10px',
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center'
     }}>
        {posts?.map((post) => {
            return(

     <RightSide key={post.post_id} post={post}/>
            )
        })}
        </Box>
    }
    <Box
    className='left'
     style={{
      width: "25%",
      padding: '25px',
     }}>
     <LeftList/>
    </Box>
   </Box>
  </div>
 );
};

export default Homepage;
